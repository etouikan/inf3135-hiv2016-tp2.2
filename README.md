# Travail pratique 2

## Description

Décrire ici le projet en quelque phrases.
Mentionner le contexte (cours, sigle, université, etc.)

## Auteur(s)

- Prénom et nom (Code permanent)
- Prénom et nom (Code permanent)
- Prénom et nom (Code permanent)

## Dépendances

Indiquez ici toutes librairies que vous avez utilisées et dont votre projet
dépend. En particulier, indiquez les versions de chacune de ces librairies (par
exemple SDL 2.0, Doxygen 1.8.10, etc.).

## Fonctionnement

Expliquez comment faire fonctionner votre projet :

- Comment le compiler;
- Comment l'exécuter;
- Comment le tester;
- Comment générer la documentation,
- etc.

## Plateformes supportées

Indiquez ici les plateformes sur lesquelles vous avez testé le projet, avec la
version exacte (par exemple MacOS 10.10.5, Debian 8.3, etc.) Dans un scénario
idéal, vous devriez tester au moins deux plateformes, mais aucune pénalité ne
sera appliquée si vous n'en testez qu'une seule (en autant qu'il s'agisse d'une
plateforme Unix!).

## Contenu du projet

Décrivez brièvement chacun des répertoires et modules contenus dans le
projet. Utilisez le format suivant :

- `nom du fichier1` : décrire le contenu du fichier1;
- `nom du fichier2` : décrire le contenu du fichier2;
- etc.

## Division des tâches

Pendant le projet, vous devez indiquer dans cette section la liste des
tâches à effectuer (*todo list*), et qui est responsable de chacune des
tâches. Utilisez le format suivant :

- [X] Tâche 1 (nom du responsable de la tâche)
- [ ] Tâche 2 (nom du responsable de la tâche)
- [ ] Tâche 3 (nom du responsable de la tâche)
- etc.

(Un X indique que la tâche est complétée, alors que des crochets vides
indiquent que la tâche n'est pas complétée)

## Références

Citez vos sources ici.

## Statut

Indiquer si le projet est complété, si tout est fonctionnel, etc.
